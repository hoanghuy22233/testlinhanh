import 'package:applinhanh/presentation/router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigatePopUtil({String name}) async {
    Navigator.popUntil(Get.context, ModalRoute.withName(name));
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.toNamed(BaseRouter.LOGIN);
    return result;
  }

  static navigateLoginOrRegister() async {
    var result = await Get.toNamed(BaseRouter.LOGIN_REGISTER);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.offAllNamed(BaseRouter.NAVIGATION);
    return result;
  }

  static navigateRegister() async {
    var result = await Get.toNamed(BaseRouter.REGISTER);
    return result;
  }

  static navigateRegisterVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.REGISTER_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }

  static navigateForgotPassword() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD);
    return result;
  }

  static navigateForgotPasswordVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }

  static navigateForgotPasswordReset({String username, String otpCode}) async {
    var result =
        await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }

  static navigatePostNews() async {
    var result = await Get.toNamed(BaseRouter.CREATE_NEWS);
    return result;
  }
}
