import 'package:applinhanh/model/api/response/base_response.dart';
import 'package:applinhanh/model/entity/news_data_data.dart';
import 'package:json_annotation/json_annotation.dart';

part 'news_response.g.dart';

@JsonSerializable()
class NewsResponse extends BaseResponse {
  NewsDataData data;

  NewsResponse(this.data);

  factory NewsResponse.fromJson(Map<String, dynamic> json) =>
      _$NewsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$NewsResponseToJson(this);
}
