import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class CartRepository {
  final Dio dio;

  CartRepository({@required this.dio});
}
