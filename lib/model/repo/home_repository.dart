import 'package:dio/dio.dart';
import 'package:meta/meta.dart';

class HomeRepository {
  final Dio dio;

  HomeRepository({@required this.dio});
}
