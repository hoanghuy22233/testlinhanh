import 'package:applinhanh/model/repo/barrel_repo.dart';
import 'package:applinhanh/presentation/screen/forgot_password/sc_forgot_password.dart';
import 'package:applinhanh/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:applinhanh/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:applinhanh/presentation/screen/login/login.dart';
import 'package:applinhanh/presentation/screen/login_register/login_register.dart';
import 'package:applinhanh/presentation/screen/menu/new/bloc/post_bloc.dart';
import 'package:applinhanh/presentation/screen/navigation/sc_navigation.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/bloc/post_form_bloc.dart';
import 'package:applinhanh/presentation/screen/product_post/sc_post.dart';
import 'package:applinhanh/presentation/screen/register/sc_register.dart';
import 'package:applinhanh/presentation/screen/register_verify/sc_register_verify.dart';
import 'package:applinhanh/presentation/screen/splash/sc_splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String LOGIN_REGISTER = '/login_register';
  static const String REGISTER = '/register';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String FORGOT_PASSWORD = '/forgot_password';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String CREATE_NEWS = 'create_news';

  static const String NAVIGATION = '/navigation';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case LOGIN_REGISTER:
        return MaterialPageRoute(builder: (_) => LoginOrRegister());

      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    var notificationRepository =
        RepositoryProvider.of<NotificationRepository>(context);
    var addressRepository = RepositoryProvider.of<AddressRepository>(context);
    var cartRepository = RepositoryProvider.of<CartRepository>(context);
    var paymentRepository = RepositoryProvider.of<PaymentRepository>(context);
    var invoiceRepository = RepositoryProvider.of<InvoiceRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreen(),
      LOGIN_REGISTER: (context) => LoginOrRegister(),
      REGISTER: (context) => RegisterScreen(),
      REGISTER_VERIFY: (context) => RegisterVerifyScreen(),
      FORGOT_PASSWORD: (context) => ForgotPasswordScreen(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      // NAVIGATION: (context) => NavigationScreen(),
      CREATE_NEWS: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) => PostFormBloc(userRepository: userRepository),
            ),
          ], child: PostScreen()),
      NAVIGATION: (context) => MultiBlocProvider(providers: [
            BlocProvider(
              create: (context) => PostBloc(userRepository: userRepository),
            ),
          ], child: NavigationScreen()),
    };
  }
}
