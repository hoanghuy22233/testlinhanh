import 'dart:io';

import 'package:equatable/equatable.dart';

class PostFormEvent extends Equatable {
  const PostFormEvent();

  List<Object> get props => [];
}

class InitPostForm extends PostFormEvent {
  @override
  String toString() {
    return 'InitPaymentForm{}';
  }
}

class ImageLoaded extends PostFormEvent {
  final List<File> image;

  ImageLoaded({this.image});

  List<Object> get props => [image];

  @override
  String toString() {
    return 'ImageLoaded{image: $image}';
  }
}

class NamesChanged extends PostFormEvent {
  final String name;

  NamesChanged({this.name});

  List<Object> get props => [name];

  @override
  String toString() {
    return 'NameChanged{name: $name}';
  }
}

class PostFormSubmitted extends PostFormEvent {
  PostFormSubmitted();

  @override
  String toString() {
    return 'PaymentFormSubmitted{}';
  }
}
