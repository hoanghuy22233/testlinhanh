import 'package:applinhanh/app/constants/color/color.dart';
import 'package:applinhanh/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/bloc/post_form_bloc.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/bloc/post_form_state.dart';
import 'package:applinhanh/presentation/screen/product_post/product_post_form/widget_post_form.dart';
import 'package:applinhanh/presentation/screen/product_post/widget_post_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class PostScreen extends StatefulWidget {
  @override
  _PostScreenState createState() => _PostScreenState();
}

class _PostScreenState extends State<PostScreen>
    with AutomaticKeepAliveClientMixin<PostScreen> {
  PanelController _panelController = new PanelController();

  @override
  Widget build(BuildContext context) {
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: Container(
          child: _buildContent(),
        ),
      ),
    );
  }

  Widget _buildContent() {
    return Container(
        child: Column(children: [
      _buildAppbar(),
      Expanded(child: BlocBuilder<PostFormBloc, PostFormState>(
        builder: (context, state) {
          return SingleChildScrollView(
              child: Container(
            color: AppColor.WHITE,
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 0),
                  width: double.infinity,
                  color: AppColor.WHITE,
                  child: _buildForm(),
                ),
              ],
            ),
          ));
        },
      ))
    ]));
  }

  Widget _buildAppbar() => WidgetPostAppbar();

  Widget _buildForm() => WidgetPostsForm();

  @override
  bool get wantKeepAlive => true;
}
