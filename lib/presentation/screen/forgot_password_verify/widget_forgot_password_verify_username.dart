import 'package:applinhanh/app/constants/style/style.dart';
import 'package:applinhanh/presentation/common_widgets/widget_spacer.dart';
import 'package:applinhanh/utils/common/common_utils.dart';
import 'package:applinhanh/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetForgotPasswordVerifyUsername extends StatelessWidget {
  final String username;

  const WidgetForgotPasswordVerifyUsername({Key key, @required this.username})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            AppLocalizations.of(context).translate('register_verify.otp'),
            style: AppStyle.DEFAULT_LARGE,
          ),
          WidgetSpacer(
            height: 15,
          ),
          Text(
            '${AppCommonUtils.hideUserName(username)}',
            style: AppStyle.DEFAULT_LARGE,
          ),
          WidgetSpacer(
            height: 10,
          ),
          Center(
            child: Text(
              AppLocalizations.of(context)
                  .translate('register_verify_notifications'),
              style: AppStyle.DEFAULT_LARGE,
            ),
          ),
        ],
      ),
    );
  }
}
