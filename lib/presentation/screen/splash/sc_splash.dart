import 'package:applinhanh/app/constants/barrel_constants.dart';
import 'package:applinhanh/model/repo/user_repository.dart';
import 'package:applinhanh/presentation/common_widgets/widget_circle_progress.dart';
import 'package:applinhanh/presentation/common_widgets/widget_logo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    openLogin(userRepository);
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            child: Image.asset(
              'assets/images/background_linh_anh.jpg',
              fit: BoxFit.fill,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [_buildLogo(), _buildProgress()],
          )
        ],
      ),
    );
  }

  _buildLogo() => WidgetLogo(
        height: Get.width * 0.63,
        widthPercent: 0.63,
      );

  _buildProgress() => WidgetCircleProgress();

  void openLogin(UserRepository repository) async {
    Future.delayed(Duration(seconds: 3), () {
      AppNavigator.navigateLoginOrRegister();
    });
  }
}
