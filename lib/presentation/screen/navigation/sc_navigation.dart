import 'package:applinhanh/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:applinhanh/presentation/screen/drawer/app_drawer.dart';
import 'package:applinhanh/presentation/screen/menu/new/sc_new.dart';
import 'package:applinhanh/presentation/screen/menu/sc_home.dart';
import 'package:applinhanh/presentation/screen/menu/sc_information.dart';
import 'package:applinhanh/presentation/screen/menu/sc_person.dart';
import 'package:applinhanh/presentation/screen/menu/sc_store.dart';
import 'package:flutter/material.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String store = '/store';
  static const String information = '/info';
  static const String news = '/news';
  static const String account = '/news';
}

class NavigationScreen extends StatefulWidget {
  NavigationScreen();

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  List<FABBottomNavItem> _navMenus = List();
  PageController _pageController;
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  bool appbar = true;

  // CAN CO DU LIEU ;;

  // _NavigationScreenState({this.type});

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        // appBar: appbar
        //     ? AppBar(
        //         backgroundColor: Colors.blue,
        //         actions: <IconButton>[
        //           IconButton(
        //             icon: Icon(Icons.search),
        //             onPressed: () {
        //               // AppNavigator.navigateSearch();
        //             },
        //           ),
        //         ],
        //       )
        //     : null,
        bottomNavigationBar: WidgetFABBottomNav(
          backgroundColor: Colors.red,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          items: _navMenus,
          selectedColor: Colors.white,
          color: Colors.grey,
        ),
        drawer: AppDrawer(
          drawer: _drawerKey,
          menu: goToPage,
          notification: goToPage,
          account: goToPage,
        ),
        body: Column(
          children: [
            _selectedIndex == 3
                ? Container()
                : Container(
                    child: Stack(
                      children: [
                        Container(
                          color: Colors.red,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height * 0.13,
                        ),
                        Positioned.fill(
                          top: MediaQuery.of(context).size.height * 0.13 / 4,
                          left: 10,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                flex: 1,
                                child: GestureDetector(
                                  onTap: () {
                                    _drawerKey.currentState.openDrawer();
                                  },
                                  child: Image.asset(
                                    'assets/images/drawer.png',
                                    width: 30,
                                    height: 30,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                flex: 8,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(400),
                                  child: Container(
                                    height: 40,
                                    color: Colors.white,
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 15),
                                        child: Text(
                                          'Tìm sản phẩm',
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 1,
                                child: Image.asset(
                                  'assets/images/bells.png',
                                  width: 30,
                                  height: 30,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                flex: 1,
                                child: Image.asset(
                                  'assets/images/shopping.png',
                                  width: 30,
                                  height: 30,
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
            Expanded(
                child: PageView(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              onPageChanged: (newPage) {
                setState(() {
                  this._selectedIndex = newPage;
                });
              },
              children: [
                HomePage(),
                StorePage(),
                InformationPage(),
                WallPage(),
                PersonPage(),
              ],
            ))
          ],
        ));
  }

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/homes.png',
          text: "Home"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.store,
          tabItem: TabItem.store,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/store.png',
          text: "Store"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.information,
          tabItem: TabItem.info,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/icon_logo.png',
          text: "Information"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.news,
          tabItem: TabItem.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/list.png',
          text: "News"),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.news,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/person.png',
          text: "Person"),
    ]);
  }

  void goToPage({int page, int id = 0}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }
}
