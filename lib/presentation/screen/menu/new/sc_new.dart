import 'package:applinhanh/app/constants/barrel_constants.dart';
import 'package:applinhanh/app/constants/color/color.dart';
import 'package:applinhanh/presentation/screen/menu/new/widget_posts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/post_bloc.dart';
import 'bloc/post_event.dart';

class WallPage extends StatefulWidget {
  WallPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _WallPage createState() => _WallPage();
}

class _WallPage extends State<WallPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<PostBloc>(context).add(LoadPost());
  }

  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.red,
    ));
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Stack(
              children: [
                Container(
                  color: Colors.red,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.12,
                ),
                Positioned.fill(
                    top: 24,
                    child: Center(
                      child: Text(
                        "Bảng tin",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Container(
            child: Stack(
              children: [
                Container(
                  color: Colors.grey[100],
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.08,
                ),
                Positioned.fill(
                  // top: MediaQuery.of(context).size.height * 0.13 / 4,
                  left: 10,
                  child: GestureDetector(
                    onTap: () {
                      AppNavigator.navigatePostNews();
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: Image.asset(
                            'assets/images/img_gift.png',
                            height: 40,
                          ),
                        ),
                        Expanded(
                          flex: 8,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(400),
                            child: Container(
                              height: 40,
                              color: Colors.white,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 15),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 8,
                                        child: Text("Tin của bạn"),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Icon(
                                          Icons.camera_alt,
                                          size: 20,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: RefreshIndicator(
                onRefresh: () async {
                  BlocProvider.of<PostBloc>(context).add(RefreshPost());
                  await Future.delayed(Duration(seconds: 2));
                  return true;
                },
                color: AppColor.PRIMARY_COLOR,
                backgroundColor: AppColor.THIRD_COLOR,
                child: _buildContent()),
          ),
        ],
      ),
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  Widget _buildContent() => WidgetPostListItem();
}
